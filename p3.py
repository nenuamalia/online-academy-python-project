# nama file p3.py
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded

# untuk revisi dan resubmisi sebelum deadline
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0
import math
priority = 0

# netacad email cth: 'abcd@gmail.com'
email = 'pristinashop@gmail.com'

# copy-paste semua #Graded cells YANG SUDAH ANDA KERJAKAN di bawah ini
# Graded


def caesar_encript(txt, shift):
    pass
    # Mulai Kode anda di sini
    result = ""
    for i in range(len(txt)):
        char = txt[i]
        # Encrypt uppercase characters in plain text
        if(char.isupper()):
            result += chr((ord(char) + shift-65) % 26 + 65)
        elif(char.islower()):
            result += chr((ord(char) + shift - 97) % 26 + 97)
        elif((ord(char) >= 32 and ord(char) <= 64) or (ord(char) >= 91 and ord(char) <= 96) or (ord(char) >= 123 and ord(char) <= 126)):
            result += chr(ord(char))
    return result


# Fungsi Decript caesar
def caesar_decript(chiper, shift):
    return caesar_encript(chiper, -shift)


msg = 'Haloz DTS mania, MANTAPSZZZ!'
cpr = caesar_encript(msg, 4)
txt = caesar_decript(cpr, 4)

print('plain text:', txt)
print('chiper text:', cpr)

# Graded

# Fungsi mengacak urutan


def shuffle_order(txt, order):
    return ''.join([txt[i] for i in order])

# Fungsi untuk mengurutkan kembali sesuai order


def deshuffle_order(sftxt, order):
    pass
    return ''.join([sftxt[order.index(i)] for i in sorted(order)])


print(shuffle_order('abcd', [2, 1, 3, 0]))
print(deshuffle_order('cbda', [2, 1, 3, 0]))

# Graded


# convert txt ke dalam bentuk list teks yang lebih pendek
# dan terenkrispi dengan urutan acak setiap batchnya
def send_batch(txt, batch_order, shift=3):
    pass
    text = caesar_encript(txt, shift)
    n = len(batch_order)

    # substring for 4 words with space
    subs = [text[i:i+n] for i in range(0, len(text), n)]

    # if not 4 add underscore and shuffle text
    pad_txt = []
    for x in subs:
        pad_txt.append(shuffle_order(x.ljust(n, "_"), batch_order))
    return pad_txt

# batch_cpr: list keluaran send_batch
# fungsi ini akan mengembalikan lagi ke txt semula


def receive_batch(batch_cpr, batch_order, shift=3):
    batch_txt = [caesar_decript(deshuffle_order(
        i, batch_order), shift) for i in batch_cpr]
    return ''.join(batch_txt).strip('_')


# Sanity check!!!
msg_cpr = send_batch('#MakinJagoDigital', [3, 0, 5, 1, 4, 2], 55)
msg_txt = receive_batch(msg_cpr, [3, 0, 5, 1, 4, 2], 55)
print(msg_txt, msg_cpr, sep='\n')
