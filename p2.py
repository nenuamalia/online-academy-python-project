# nama file p2.py 
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded

# untuk revisi dan resubmisi sebelum deadline
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0
priority = 0

#netacad email cth: 'abcd@gmail.com'
email='pristinashop@gmail.com'
 
# copy-paste semua #Graded cells YANG SUDAH ANDA KERJAKAN di bawah ini

#Graded

def isPointInCircle(x,y,r,center=(0,0)):
  hitungan = abs(((x - center[0])**2) + ((y-center[1])**2))
  r2 = abs((r)**2)
  if hitungan < r2:
      return True
  elif hitungan > r2:
      return False
  elif hitungan == r2:
      return True
  pass

print(isPointInCircle(1,1,1,center=(0,0)),isPointInCircle(1,0,1,center=(0,0)),
      isPointInCircle(1,1,1,center=(1,0)),isPointInCircle(0,0,1,center=(1,1)))

#Graded

import random

def generateRandomSquarePoints(n,length,center=(0,0)):
  # MULAI KODEMU DI SINI
  minx = center[0]-length/2
  miny = center[1]-length/2
  
  # Gunakan list comprehension dengan variable minx, miny, length, dan n
  points = [[random.uniform(minx, minx+length),random.uniform(miny, miny+length)] for i in range(n)]

  return points

#CEK OUTPUT KODE ANDA
random.seed(0)

# generate 100 point di dalam persegi dengan panjang sisi 1 dan berpusat di (0,0)
points = generateRandomSquarePoints(100,1)
print(points[10:15])

#Graded

def MCCircleArea(r,n=100,center=(0,0)):
  length = r*2
  pointInside = 0
  for x,y in generateRandomSquarePoints(n,length,center):
      if(isPointInCircle(x,y,r,center)) == True:
        pointInside += 1
  luas_lingkaran = (pointInside/n)*(length*length)
  return luas_lingkaran
  pass

#CEK OUTPUT KODE ANDA

random.seed(0)
print(MCCircleArea(1,100),MCCircleArea(1,10,center=(10,10)))


#Graded

def LLNPiMC(nsim,nsample):
  r = 1
  total_luas = 0
  for i in range(nsim):
    luas_lingkaran = MCCircleArea(r,nsample)
    total_luas = luas_lingkaran + total_luas
  return total_luas/nsim
  pass


import math

random.seed(0)
estpi = LLNPiMC(10000,500)

print('est_pi:',estpi)
print('act_pi:',math.pi)

# Graded
def relativeError(act,est):
  e = abs((est-act)/act)*100
  return e
  pass

#CEK OUTPUT KODE ANDA
print('error relatif:',relativeError(math.pi,estpi),'%')
